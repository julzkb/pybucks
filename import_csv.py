import csv
import sys
import sheets
import time
import logging
import argparse
from tqdm import tqdm
from datetime import datetime

# The archive file will store the past transactions to avoid duplicates
ARCHIVE = 'archive.csv'
logging.basicConfig(level='INFO')


def get_csv_data(infile):
    '''
    Get a csv file and returns its data
    Return : Data (lst)
    '''

    data = []
    with open(infile) as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',')
        for row in csv_reader:
            data.append(row)

    return data


def is_cc(d):
    """
    Checks if the csv data is credit card
    Returns: Bool
    """
    cd = []

    for i in d:

        if (("C" or "D" in i[1]) and len(i[1]) == 1):
            cd.append(1)

    if len(cd) > 1:
        return True

    else:
        return False


def parse_cc(d):
    """
    Parse credit card dataset
    Returns : Parsed list
    """
    data = []
    # remove all "converted" data

    # Ignore C
    for i in d:
        if "C" in i[1]:
            pass
        else:
            # take last column and make it the index
            if i[2] != "":
                data.append((i[4], i[3], float(i[2])*-1, "Credit Card"))
            
    return data


def parse(d):
    """
    Parse normal statement
    Returns: Parsed list of transactions
    """
    data = []

    for i in d:

        if "4835" in i[1]:
            description = '-'.join([i[2], i[3]])
        else:
            description = '-'.join([i[1], i[2], i[3]])
        data.append((i[6], description.lstrip('-'), float(i[5]), "Freedom"))

    return data

def parse_access(d):
    """
    Parse normal access statement
    Returns: Parsed list of transactions
    """
    data = []

    for i in d:
        if len(i) > 0:
            description = ''
            if 'VISA DEBIT PURCHASE CARD' in i[2]:
                description = i[2].replace('VISA DEBIT PURCHASE CARD','')
            elif 'EFTPOS' in i[2]:
                description = i[2].replace('EFTPOS','')
            else:
                description = i[2]

            # print(description)
            data.append((i[0], description, float(i[1]), "Access"))

    # print(data)
    # sys.exit(0)
    return data



def find_categories(s, c):
    '''
    Matches the categories from the categorie sheet in Google Spreadsheets with the input string
    Returns : matched category
    '''
    for x in c:
        if any(z in s for z in c[x]):
            return x
            break
        else:
            pass


def assign_categories(d, currency):
    """
    Tries to assign categories based on descriptions and category dict
    Returns: List with categories
    """
    categories = sheets.get_categories()
    categorised_data = []

    for i in d:
        category_found = find_categories(i[1].lower(), categories)

        if category_found is not None:
            categorised_data.append(i + (currency, category_found, ))
            # categorised_data.append(i + (category_found, ))

        else:
            # categorised_data.append(i)
            categorised_data.append(i + (currency, ))


    return categorised_data


def write_cleaned_csv(data, name):
    '''
    Writes a cleaned out csv file fire
    Inputs : data, name
    Returns : none
    '''
    cleaned_csv_file = name
    with open(cleaned_csv_file, 'w') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(assign_categories(data))

    csvFile.close()


def get_archive_data():
    '''
    Reads archive csv file
    Input : None
    Returns : archive list
    '''
    archive_data = []
    with open(ARCHIVE, 'r') as archive_csv:
        csv_reader = csv.reader(archive_csv, delimiter=',')
        for row in csv_reader:
            archive_data.append(row)

    archive_csv.close()

    return archive_data


def send_to_archive(data):
    '''
    Appends transactions to existing archive
    Input : data
    Returns : None
    '''

    with open(ARCHIVE, 'a') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(data)

    csvFile.close()


def dedup(data):
    '''
    Checks if transaction is already present in the archive, skips transaction if it is .
    Input : data
    Returns : only new data
    '''
    archive_data = sheets.get_data()
    new_data, clean_archive_data = [], []

    print(archive_data)
    for z in archive_data:
        if "$" in z[2]:
            print(z)
            price = int(float((z[2].replace("$", "").replace(",", ''))))
            x = ','.join([z[0], z[1], str(price), z[3]])
            # x = ','.join([z[0], z[1], str(price)])
            # x = ','.join([str(int(x[2])).strip().replace('$', '') for x in z])
            clean_archive_data.append(x)

    # for x in clean_archive_data:
    #     print(x)

    for d in data:
        price = int(float(d[2]))
        y = ','.join([d[0], d[1], str(price), d[3]])

        if y in clean_archive_data:
            # logging.info(
            #     f'{d} is already present in spreadsheet, skipping ...')
            pass
        else:
            logging.info(f'Adding : {d}')

            new_data.append(d)

    return new_data


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("csvfile",
                        help="csv file you want to import to Google Sheets")
    args = parser.parse_args()

    data = get_csv_data(args.csvfile)

    filename = str(args.csvfile.split('/')[-1])
    print(filename)
    if 'Transactions' in filename:
        currency = 'NZD'
    else:
        currency = 'AUD'

    if currency == 'AUD':
        data = parse_access(data)

    else:
        if is_cc(data) is True:
            data = parse_cc(data)
        else:
            data = parse(data)

    dedup_data = dedup(data)

    for i in tqdm(assign_categories(dedup_data, currency)):
        logging.info(i)
        sheets.write_row(i)  # send to google sheets
        time.sleep(2)  # be nice to the google API


if __name__ == "__main__":
    main()
