## PyBucks

This is just a quick script to parse *csv* transaction data from my bank and categorise transactions automatically based on their names . Cleaned transactions are then sent to a *Google Spreadsheet* .

You are going to need a Google API access token pickle file, [here](https://developers.google.com/sheets/api/quickstart/python) are the instructions from the Google Sheets API doc .
*Note that you need to create the token file locally as it will fire up a browser window, you can then copy the file around if you need to .*

This is obviously tailored to the transaction format of my local bank, so your mileage may vary .


