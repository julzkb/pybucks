import unittest
import import_csv

# TODO : finish writing tests

class TestImportCsv(unittest.TestCase):

    data1 = [['*****-*****', 'D', "50,20", "some date", "some description"],
            ['*****-*****', 'C', "11.2", "some other date", "some description"]]

    data2 = [['*****-*****', '', "50,20", "some date", "some description"],
        ['*****-*****', '', "11.2", "some other date", "some description"]]

    def test_get_csv_data(self):
        x = import_csv.get_csv_data('test_data/credit.csv')
        self.assertGreater(len(x), 1)

    def test_is_cc1(self):
        x = import_csv.is_cc(self.data1)
        self.assertIs(x, True)

    def test_is_cc2(self):
        x = import_csv.is_cc(self.data2)
        self.assertIs(x, False)


if __name__ == "__main__":
    unittest.main()