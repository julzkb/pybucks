from __future__ import print_function
import pickle
import os.path
import logging
import os
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

logging.basicConfig(level='INFO')

SPREADSHEET_ID = os.environ['SPREADSHEET_ID']
RANGE_NAME = 'Categories!A:AR'
DATA_RANGE = 'Data!A:D'



def sheet_login():
    # If modifying these scopes, delete the file token.pickle.
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

    # The ID and range of a the target spreadsheet

    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)

    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds, cache_discovery=False)
    return service.spreadsheets()


def get_categories():
    '''
    Get the categories from Google Spreadsheet
    Input : None
    Returns : Dict with categories and taxons
    '''
    sheet = sheet_login()
    result = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                                majorDimension='COLUMNS',
                                range=RANGE_NAME).execute()
    values = result.get('values', [])
    categories = {}
    for v in values:
        categories[v[0]] = v[1:]

    return categories


def get_data():
    '''
    Get the categories from Google Spreadsheet
    Input : None
    Returns : Dict with categories and taxons
    '''
    sheet = sheet_login()
    result = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                                majorDimension='ROWS',
                                range=DATA_RANGE).execute()
    values = result.get('values', [])

    return values


def write_row(data):
    '''
    Writes row to google spreadsheet
    Input : row data
    Returns : prints the result
    '''

    sheet = sheet_login()
    row = {'values': [data]}
    result = sheet.values().append(spreadsheetId=SPREADSHEET_ID,
                                   range='Data',
                                   valueInputOption='USER_ENTERED',
                                   body=row).execute()
    logging.info(result)


if __name__ == "__main__":
    sheet_login()